const express = require('express');
const graphqlHTTTP = require('express-graphql');
const schema = require('./schema/graphql.js');
const mongoose =require('mongoose');
const cors =require("cors");

const app = express();

app.use(cors());


mongoose.connect('mongodb+srv://elodie:elodie@cluster0-werjr.mongodb.net/graphreact');
mongoose.connection.once('open',()=>{
	console.log('connected to db');
});

app.use('/graphql',graphqlHTTTP({
	schema,
	graphiql:true
}));

app.listen(3030,()=>{
	console.log('server running');
});