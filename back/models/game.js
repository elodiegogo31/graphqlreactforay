const mongoose= require('mongoose');
const Schema = mongoose.Schema;


const gameSchema= new Schema({
	name: String,
	category: String,
	mode: String,
	platform: String,
	pg_rating: String,
	release_year: Number

});


module.exports = mongoose.model("Game", gameSchema);