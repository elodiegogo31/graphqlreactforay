const graphql = require('graphql');
const Character = require("../models/character.js");
const Game = require("../models/game.js");

const {GraphQLObjectType, GraphQLList, GraphQLInt, GraphQLID, GraphQLString, GraphQLSchema} = graphql;

/*

var characters =[
	{
		id:'1',
		name:"Gandalf",
		species:"Magician",
		gameId:"5"
	},
	{	
		id:'2',
		name:"Spiro",
		species:"Dragon",
		gameId:"6"
	},
	{	
		id:'3',
		name:"Pikachu",
		species:"Pokemon",
		gameId:"7"
	},
	{
		id:'4',
		name:"Mario",
		species:'Human',
		gameId:"4"

	}
];

var games =[
	{
		id:'1',
		title:'Diablo',
		category: 'hack and slask',
		platform:'pc'
		release_Date:'2000',
		pg_rating: '18+',
	},
	{
		id:'2',
		title:'Call of Duty',
		category: 'FPS',
		platform: 'pc',
		release_Date:'2002',
		pg_rating: '18+',
	},
	{
		id:'3',
		title:'World of Warcraft',
		category: 'MMORPG',
		platform: 'online',
		release_Date:'2002',
		pg_rating: '18+',
	},
	{
		id:'4',
		title:'Mario Cart',
		category: 'racing',
		platform: 'nintendo',
		release_Date:'2002',
		pg_rating: '7+',
	},
		{
		id:'5',
		title:'Lord Of The Rings',
		category: 'fantasy',
		platform: 'pc',
		release_Date:'2002',
		pg_rating: '12+',
	},
		{
		id:'6',
		title:'Spiro',
		category: 'adventure',
		platform: 'playstation',
		release_Date:'2002',
		pg_rating: '7+',
	},
		{
		id:'7',
		title:'Pokemon',
		category: 'adventure',
		platform: 'playstation',
		release_Date:'2002',
		pg_rating: '7+',
	},
];
*/


const GameType = new GraphQLObjectType({
	name:"Game",
	fields: ()=>({
		id: {type: GraphQLID},
		name: {type: GraphQLString},
		platform:{type: GraphQLString},
		category:{type: GraphQLString},
		release_year:{type: GraphQLInt},
		pg_rating:{type: GraphQLString},
		characters:{
			type: new GraphQLList(CharacterType),
			resolve(parent,args){
				return Character.find({gameId: parent.id});
			}
		}
	})
});



const CharacterType = new GraphQLObjectType({
	name:"Character",
	fields: ()=>({
		id: {type: GraphQLID},
		name: {type: GraphQLString},
		species: {type: GraphQLString},
		game: {
			type: GameType,
			resolve(parent,args){
				return Game.findById(parent.gameId);
			}
		}

	})
});

const RootQuery = new GraphQLObjectType({
	name:"RootQueryType",
	fields:{
		character:{
			type: CharacterType,
			args:{id:{type:GraphQLID}},
			resolve(parent,args){
				return Character.findById(args.id);
			}
		},
		game:{
			type: GameType,
			args:{id:{type:GraphQLID}},
			resolve(parent,args){
				return Game.findById(args.id);
			}
		},
		characters:{
			type: new GraphQLList(CharacterType),
			resolve(parent,args){
				console.log(Character.find({}))
				return Character.find({});
			}
		},
		games:{
			type: new GraphQLList(GameType),
			resolve(parent, args){
				return Game.find({});
			}
		}
	}
});

const Mutation = new GraphQLObjectType({
	name:'Mutation',
	fields:{
		addGame:{
			type: GameType,
			args: {
				name: {type:GraphQLString},
				category: {type:GraphQLString},
				mode:{type: GraphQLString},
				platform:{type:GraphQLString},
				pg_rating:{type:GraphQLString},
				release_year:{type:GraphQLInt}
			},
			resolve(parent,args){
				let game = new Game({
					name:args.name,
					category:args.category,
					mode:args.mode,
					platform:args.platform,
					pg_rating:args.pg_rating,
					release_year:args.release_year
				});
				return game.save();
			}
		},
		addCharacter:{
			type: CharacterType,
			args:{
				name: {type:GraphQLString},
				species: {type:GraphQLString},
				gameId:{type:GraphQLID}
			},
			resolve(parent,args){
				let character = new Character({
					name:args.name,
					species:args.species,
					gameId:args.gameId
				});
				return character.save();
			}
		},

	}
})

module.exports = new GraphQLSchema({
	query:RootQuery,
	mutation:Mutation
});