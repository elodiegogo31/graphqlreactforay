import React, {Component} from 'react';
import '../App.css';
import {graphql} from 'react-apollo';
import {getSpecificCharacter} from '../Queries/queries';


class CharacterDetails extends Component {


	displayCharDetails(){
		var {character} = this.props.data;
		if(character){
			return(<div><h2>{character.name}</h2><span>{character.species}</span> {character.game?(<p>game: {character.game.name} rating: {character.game.pg_rating}</p>):(<p>Pas de jeu associé</p>)}</div>)
		}else{
			return(<div>No details on character</div>)
		}
	}
	
	render(){
  		return (
   			<div className="">
   			{this.displayCharDetails()}
    		</div>
		);
	}
}

export default graphql(getSpecificCharacter,{
	options:(props)=>{
		return{
			variables:{
				id: props.charId
			}
		}
	}
})(CharacterDetails);