import React, {Component} from 'react';
import '../App.css';
import {graphql} from 'react-apollo';
import {getCharacters} from '../Queries/queries';

import CharacterDetails from './CharacterDetails';

class CharacterList extends Component {
	constructor(props){
		super(props);
		this.state={
			charRequested:null
		}
	}
	displayCharacters(){
		var characters = this.props.data;
		if(characters.loading){
			return( <div>loading</div>);
		}else{
			let randomNums = [];
			let i= 0;
			while(i<4){
			let num = Math.floor(Math.random() * characters.characters.length)
			randomNums.push(num);
			i++;
			}
			console.log('randomnums' + randomNums)
			var randomChars= [];
			for(let x=0; x<randomNums.length ; x++){
				console.log('debug' + [randomNums[x]] + characters.characters[randomNums[x]])
			let char = characters.characters[randomNums[x]];
			randomChars.push(char);
			}
		
			console.log('randomchars' + randomChars)
			return randomChars.map(char => {
				return(
					<li onClick={(e)=>{this.setState({charRequested: char.id})}} key={char.id}>{char.name} <span>{char.species}</span></li>
				)
			})
		}

	}
	render(){
  		return (
   			<div className="characterListing">
   				<ul id="characterList">
   					{this.displayCharacters()}
   				</ul>
   				<CharacterDetails charId={this.state.charRequested}/>
    		</div>
		);
	}
}

export default graphql(getCharacters)(CharacterList);