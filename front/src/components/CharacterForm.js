import React, {Component} from 'react';
import '../App.css';
import {graphql} from 'react-apollo';
import {getGames, getCharacters, addCharacter} from '../Queries/queries';
import * as compose from 'lodash.flowright';


class CharacterForm extends Component {
	constructor(props){
		super(props);
		this.state={
			name:"",
			species:"",
			gameId:"",

		};
	}

	displayKnownGames(){
		var data = this.props.getGames;
		if(data.loading){
			return(<option>Games have yet to be added to the database</option>)
		}else{
			return data.games.map(game =>{
				return(<option key={game.id} value={game.id}>{game.name}</option>)
			})
		}
	}
	submitForm(e){
		e.preventDefault();
		this.props.addCharacter({
			variables:{
				name: this.state.name,
				species:this.state.species,
				gameId:this.state.gameId
			},
			refetchQueries:[{query: getCharacters}]
		})

	}
	render(){
		console.log(this.props);
  		return (
   			<div className="">
   				<form id="create-character" onSubmit={this.submitForm.bind(this)}>
   					<div>
   						<label>Name: </label>
   						<input type="text" onChange={(e)=> this.setState({name: e.target.value})}/>
   					</div>
   					<div>
   						<label>Species: </label>
   						<input type="text" onChange={(e)=> this.setState({species: e.target.value})}   />
   					</div>
   						<div>
   						<label>Game: </label>
   						<select onChange={(e)=> this.setState({gameId: e.target.value})} >
   							<option>Select from the list</option>
   							{ this.displayKnownGames() }
   						</select>
   					</div>
   					<button>+</button>

   				</form>
    		</div>
		);
	}
}

export default compose(
	graphql(getGames,{name:"getGames"}),
	graphql(addCharacter,{name:"addCharacter"})

)(CharacterForm);