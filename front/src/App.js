import React from 'react';
import './App.css';
import ApolloClient from 'apollo-boost';
import {ApolloProvider} from 'react-apollo';

import CharacterList from'./components/CharacterList';
import CharacterForm from'./components/CharacterForm';



const client = new ApolloClient({
  uri: 'http://localhost:3030/graphql'
})




function App() {
  return (
    <ApolloProvider client={client}>
      <div className="App">
        <h1>Come up with a new concept </h1>
        <CharacterList />
        <CharacterForm />
      </div>
    </ApolloProvider>
  );
}

export default App;
