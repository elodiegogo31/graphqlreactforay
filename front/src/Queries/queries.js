import {gql} from 'apollo-boost';

const getCharacters = gql`
	{
		characters{
			name
			species
			id
		}
	}
`
const getSpecificCharacter = gql`
	query($id:ID){
		character(id: $id){
			name
			species
			id
			game{
				name
				category
				pg_rating
			}
		}
	}
`

const getGames = gql`
	{
		games{
			name
			id
		}
	}
`

const addCharacter = gql`
	mutation($name: String!, $species:String, $gameId: ID){
		addCharacter(name:$name, species:$species, gameId:$gameId){
			name
			id
		}

	}
`
export {getCharacters, getSpecificCharacter, getGames, addCharacter};